package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static String url;
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null){
			instance = new DBManager();
		}

		return instance;
	}

	private DBManager() {
		try (InputStream input = new FileInputStream("app.properties")) {
			Properties properties = new Properties();
			properties.load(input);

			url = properties.getProperty("connection.url");
			System.out.println(url);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Connection getConnection() throws SQLException {
//		String url = "jdbc:postgresql://localhost:5432/testdb?user=dev&password=dev123";
		return DriverManager.getConnection(url);
	}

	public void close(AutoCloseable closeable){
		if(closeable != null) {
			try {
				closeable.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList = new ArrayList<>();

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try{
			connection = getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(QUERIES.SELECT_ALL_USERS);

			while(resultSet.next()){
				userList.add(parseUser(resultSet));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(resultSet);
			close(statement);
			close(connection);
		}

		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try{
			connection = getConnection();
			statement = connection.prepareStatement(QUERIES.INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getLogin());

			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();

			if(resultSet.next()){
				user.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(resultSet);
			close(statement);
			close(connection);
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		try{
			connection = getConnection();
			statement = connection.prepareStatement(QUERIES.DELETE_USER);

			for(User user : users){
				statement.setInt(1, user.getId());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(statement);
			close(connection);
		}

		return true;
	}

	public User getUser(String login) throws DBException {
		User user = null;

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try{
			connection = getConnection();
			statement = connection.prepareStatement(QUERIES.SELECT_USER_BY_LOGIN);
			statement.setString(1, login);
			resultSet = statement.executeQuery();

			if(resultSet.next()){
				user = parseUser(resultSet);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(resultSet);
			close(statement);
			close(connection);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try{
			connection = getConnection();
			statement = connection.prepareStatement(QUERIES.SELECT_TEAM_BY_NAME);
			statement.setString(1, name);
			resultSet = statement.executeQuery();

			if(resultSet.next()){
				team = parseTeam(resultSet);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(resultSet);
			close(statement);
			close(connection);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList = new ArrayList<>();

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try{
			connection = getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(QUERIES.SELECT_ALL_TEAMS);

			while(resultSet.next()){
				teamList.add(parseTeam(resultSet));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(resultSet);
			close(statement);
			close(connection);
		}

		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try{
			connection = getConnection();
			statement = connection.prepareStatement(QUERIES.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, team.getName());

			statement.executeUpdate();
			resultSet = statement.getGeneratedKeys();

			if(resultSet.next()){
				team.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(resultSet);
			close(statement);
			close(connection);
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		try{
			connection = getConnection();
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(QUERIES.INSERT_USER_TEAM);
			statement.setInt(1, user.getId());

			for(Team team : teams) {
				statement.setInt(2, team.getId());
				statement.executeUpdate();
			}

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(statement);
			close(connection);
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teamList = new ArrayList<>();

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		try{
			connection = getConnection();
			statement = connection.prepareStatement(QUERIES.SELECT_USER_TEAM);
			statement.setInt(1, user.getId());
			resultSet = statement.executeQuery();

			while(resultSet.next()){
				teamList.add(parseTeam(resultSet));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(resultSet);
			close(statement);
			close(connection);
		}

		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		try{
			connection = getConnection();
			statement = connection.prepareStatement(QUERIES.DELETE_TEAM);
			statement.setInt(1, team.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(statement);
			close(connection);
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		try{
			connection = getConnection();

			statement = connection.prepareStatement(QUERIES.UPDATE_TEAM);
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());

			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		} finally {
			close(statement);
			close(connection);
		}

		return true;
	}

	private User parseUser(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getInt(QUERIES.USERS_ID));
		user.setLogin(resultSet.getString(QUERIES.USERS_LOGIN));
		return user;
	}


	private Team parseTeam(ResultSet resultSet) throws SQLException {
		Team team = new Team();
		team.setId(resultSet.getInt(QUERIES.TEAMS_ID));
		team.setName(resultSet.getString(QUERIES.TEAMS_NAME));

		return team;
	}

	private static class QUERIES{
		static final String TABLE_USERS = "users";
		static final String TABLE_TEAMS = "teams";
		static final String TABLE_USERS_TEAMS = "users_teams";

		static final String USERS_ID = "id";
		static final String USERS_LOGIN = "login";

		static final String TEAMS_ID = "id";
		static final String TEAMS_NAME = "name";

		static final String USERS_TEAMS_USER_ID = "user_id";
		static final String USERS_TEAMS_TEAM_ID = "team_id";

		static final String SELECT_USER_BY_LOGIN =
				"SELECT " +
						USERS_ID + ", " +
						USERS_LOGIN + " " +
				"FROM " + TABLE_USERS + " " +
				"WHERE " + USERS_LOGIN + "=?";

		static final String SELECT_ALL_USERS =
				"SELECT " +
						USERS_ID + ", " +
						USERS_LOGIN + " " +
				"FROM " + TABLE_USERS;

		static final String INSERT_USER =
				"INSERT INTO " + TABLE_USERS +"( " +
						USERS_LOGIN +
				") VALUES(?)";

		static final String DELETE_USER =
				"DELETE FROM " + TABLE_USERS + " " +
				"WHERE " + USERS_ID + "=?";

		static final String SELECT_TEAM_BY_NAME =
				"SELECT " +
						TEAMS_ID + ", " +
						TEAMS_NAME + " " +
						"FROM " + TABLE_TEAMS + " " +
						"WHERE " + TEAMS_NAME + "=?";

		static final String SELECT_ALL_TEAMS =
				"SELECT " +
						TEAMS_ID + ", " +
						TEAMS_NAME + " " +
				"FROM " + TABLE_TEAMS;

		static final String INSERT_TEAM =
				"INSERT INTO " + TABLE_TEAMS +"( " +
						TEAMS_NAME +
				") VALUES(?)";

		static final String INSERT_USER_TEAM =
				"INSERT INTO " + TABLE_USERS_TEAMS +"( " +
						USERS_TEAMS_USER_ID + ", " +
						USERS_TEAMS_TEAM_ID +
				") VALUES(?, ?)";

		static final String SELECT_USER_TEAM =
				"SELECT " +
						"t." + TEAMS_ID + ", " +
						"t." + TEAMS_NAME + " " +
				"FROM " + TABLE_TEAMS + " t " +
				"JOIN " + TABLE_USERS_TEAMS + " ut " +
						"ON t." + TEAMS_ID + "=ut." + USERS_TEAMS_TEAM_ID + " " +
				"WHERE ut." + USERS_TEAMS_USER_ID + "=?";

		static final String DELETE_TEAM =
				"DELETE FROM " + TABLE_TEAMS + " " +
				"WHERE " + TEAMS_ID + "=?";

		static final String UPDATE_TEAM =
				"UPDATE " + TABLE_TEAMS + " " +
				"SET " + TEAMS_NAME + "=? " +
				"WHERE " + TEAMS_ID + "=?";
	}
}
